<?php
namespace becontent;

use becontent\core\control\Settings as Settings;


if(Settings::getOperativeMode()=="release")
	error_reporting(E_ERROR);



use becontent\core\control\Config as Config;
use becontent\core\presentation\Message as Message;
use becontent\core\presentation\Parser as Parser;
use becontent\core\foundation\DB as DB;
use becontent\tags\presentation\TagLibrariesFactory as TagLibrariesFactory;
use becontent\router\Router as Router;
use becontent\resource\foundation\DoctrinePersistentManager as DoctrinePersistentManager;

Class beContent {
	var
	$persistenceManager;

	private static $instance=null;

	public static function getInstance()
	{
		if(!isset(self::$instance))
		{
			self::$instance=new beContent();
		}
		return self::$instance;
	}

	/**
	 *
	 */
	function __construct() {		
		
		$this->persistenceManager=new DoctrinePersistentManager();
		
	}

	function getPersistenceManager()
	{
		return $this->persistenceManager;
	}
		
	function resource($resourceClassifier,$resourceId)
	{
		$requestedResource=$this->persistenceManager->loadResource($resourceClassifier,$resourceId);
		return $requestedResource;
	}
	
	function findResources($resourceClassifier,$resourceCriteria=null,$orderCriteria=null)
	{
		$requestedResource=$this->persistenceManager->findResources($resourceClassifier,$resourceCriteria,$orderCriteria);
		return $requestedResource;
	}

	function setResourceMediator($resourceMediator)
	{
		$this->resourceMediator=$resourceMediator;
	}
	
	function getResourceMediator()
	{
		return $this->resourceMediator;
	}
	
	function setPersistenceMediator($persistenceMediator)
	{
		$this->persistenceMediator=$persistenceMediator;
	}
	
	function getPersistenceMediator()
	{
		return $this->persistenceMediator;
	}
	
	function injectRoutingTable($routingTable)
	{
		Router::getInstance()->injectRoutingTable($routingTable);
	}
	
	/**
	 * 
	 * @param unknown $route
	 * @param unknown $parameters
	 */
	function dispatchRoute($route,$parameters)
	{
		Router::getInstance()->dispatch($route, $parameters);
	}
}
?>
