<?php
namespace becontent;

use becontent\core\control\Settings as Settings;
use becontent\tags\presentation\TagLibrariesFactory as TagLibrariesFactory;
use becontent\tags\presentation\TagLibrary as TagLibrary;
use becontent\router\Router as Router;
use becontent\beContent;

class PluginLoader {
	/**
	 *
	 * @var unknown
	 */
	private $pluginRoot;

	/**
	 *
	 * @var unknown
	 */
	private $scriptsWhereInjected;
	
	
	/**
	 * Filepath of the Json index of the elements contained in the plugin, it's often used in order to fast retrieve
	 * files contained in deep file system structures such as TagLibraries
	 *
	 * @var unknown
	 */
	private $repositoryFilePath;
	
	
	/**
	 * The runtime elements index of the elements contained in the plugin, it's often used in order to fast retrieve
	 * files contained in deep file system structures such as TagLibraries
	 *
	 * @var unknown
	 */
	private $repository;
	
	/**
	 *
	 * @param unknown $pluginRoot        	
	 */
	function __construct($pluginRoot) {
		
		/**
		 * Initializing the repository file of the plugin
		 */
		$this->repository->README = "Copyright beContent-Team This is the repo file for the System Component, this file is used in order to increase performances in requiring and instancing tagLibraries structure for this Component";

		/**
		 * Initializing the root path for the plugin
		 */
		$this->pluginRoot = $pluginRoot;
		
		/**
		 * Initializing the root path for the plugin repository file
		 */
		$this->repositoryFilePath = $this->pluginRoot . '/pluginRepo.json';	
		
		/**
		 * Injecting the routing table of the plugin
		 */
		Router::getInstance ()->injectRoutingTable ( $this->loadScripts () );
		
		
		
		/**
		 * Loading resource classes
		 */
		$this->loadResources ();
		
		
		
		/**
		 * Including further "plugin-defined" inclusions
		 */
		if (file_exists ( $pluginRoot . '/autoload.php' )) {
			require_once $pluginRoot . '/autoload.php';
		}

		
		
		$this->loadControllers($pluginRoot."/controllers");
		
		
		
		$this->storeRepository ();
	}
	
	/**
	 * 
	 * @param unknown $pluginRoot
	 * @return boolean
	 */
	private function loadControllers($pluginRoot)
	{
		$this->repository->supportedControllers= array();
		$elements_in_dir = scandir ( $pluginRoot );
		foreach ( $elements_in_dir as $k => $v ) {
			if ($v != "." && $v != "..") {
				if (is_file ( $pluginRoot . "/" . $v )) {
							
					$controllerName=explode(".",$v);
							
							if($controllerName[1]== "class" && $controllerName[2] == "php")
							{
								require_once $pluginRoot . '/' . $v;
								$controllerClassName=$controllerName[0];
								$controllerPrototype=new $controllerClassName();
								$this->repository->supportedControllers[$controllerPrototype->getRoutingIdentifier()]=get_class($controllerPrototype);
								
							}
				} else {
					$this->loadControllers ($pluginRoot . "/" . $v);
				}
			}
		}
	}
	
	/**
	 * 	Initializes the resources found in the plugin
	 */
	public function loadResources() {
		$this->repository->supportedResources=array();
		foreach ( scandir ( realpath ( $this->pluginRoot . "/resources/" ) ) as $filename ) {
			$path = realpath ( $this->pluginRoot . '/resources/' . $filename );
			if (is_file ( $path )) {
				require_once ($path);
				$this->repository->supportedResources[]=$path;
			}
		}
	}
	
	/**
	 * Stores the repository of the plugin
	 */
	public function storeRepository() {
		
		
		file_put_contents ( $this->repositoryFilePath, json_encode ( $this->repository, JSON_PRETTY_PRINT ) );
	}
	
	
	/**
	 * DEPRECATED USE CONTROLLERS
	 * @deprecated
	 */
	public function loadScripts() {
		if (! $this->scriptsWhereInjected) {
			$this->scriptsWhereInjected = true;
			$this->repository->supportedScripts = $this->loadScriptsFromFileSystem ();
		}
		return $this->repository->supportedScripts;
	}
	
	
	/**
	 * DEPRECATED USE CONTROLLERS
	 * @return Ambigous <multitype:, string>
	 * @deprecated
	 */
	public function loadScriptsFromFileSystem() {
		$scriptsRepository = $this->pluginRoot;
		
		$supportedScripts = array ();
		$folder_contents = scandir ( $scriptsRepository );
		
		$scriptIndex = "";
		
		if (is_dir ( realpath ( $scriptsRepository . '/' . $v . '/' ) )) {
			$supportedScripts = $this->recursiveFindScriptsInDir ( $scriptsRepository . '/' . $v . '/scripts', $scriptIndex, $supportedScripts );
		}
		
		return $supportedScripts;
	}
	
	
	/**
	 * DEPRECATED USE CONTROLLERS
	 * @param unknown $dirPath        	
	 * @param unknown $scriptRoute        	
	 * @param unknown $supportedScripts        	
	 * @return string
	 * @deprecated
	 */
	public function recursiveFindScriptsInDir($dirPath, $scriptRoute, $supportedScripts) {
		$folder_contents = scandir ( $dirPath );
		
		foreach ( $folder_contents as $k => $v ) {
			if ($v != "." && $v != ".." && $v != ".svn" && $v != ".git") {
				
				$scriptIndex = $v;
				
				if (is_dir ( $dirPath . "/" . $v )) {
					$supportedScripts = $this->recursiveFindScriptsInDir ( $dirPath . "/" . $v, $scriptRoute . '/' . $scriptIndex, $supportedScripts );
				} else {
					// require_once $dirPath."/".$v;
					$supportedScripts [$scriptRoute . '/' . $scriptIndex] = realpath ( $dirPath . "/" . $v );
				}
			}
		}
		return $supportedScripts;
	}
	
	/**
	* DEPRECATED USE CONTROLLERS
	 * @return multitype:
	 */
	public function loadScriptsFromRepository() {
		$supportedScripts = get_object_vars ( $this->repository->supportedScripts );
		return $supportedScripts;
	}
}
?>
